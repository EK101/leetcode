package LeetCode;

import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;


public class LeetCode_120 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String list1 = scanner.nextLine();

        List<List<Integer>> listOfLists = stringToListOfLists(list1);

        int result = minimumTotal(listOfLists);
        System.out.println(result);

    }

    public static int minimumTotal(List<List<Integer>> triangle) {

        if (triangle.size() == 1) {
            return triangle.get(0).get(0);
        }

        for (int i = triangle.size() - 2; i >= 0; i--){
            for (int j = 0; j < triangle.get(i).size(); j++) {
                triangle.get(i).set(j, triangle.get(i).get(j) +
                        Math.min(triangle.get(i+1).get(j), triangle.get(i+1).get(j+1)));
            }
        }
        return triangle.get(0).get(0);
    }

    public static List<List<Integer>> stringToListOfLists(String inputString) {
        List<List<Integer>> listOfLists = new ArrayList<>();

        // Remove brackets and split into inner list strings
        String[] innerListStrings = inputString.substring(2, inputString.length() - 2).split("\\],\\[");

        // Process each inner list string
        for (String innerListString : innerListStrings) {
            String[] numbers = innerListString.split(",");
            List<Integer> innerList = new ArrayList<>();

            // Parse and add integers to the inner list
            for (String number : numbers) {
                innerList.add(Integer.parseInt(number));
            }

            listOfLists.add(innerList);
        }

        return listOfLists;
    }
}
