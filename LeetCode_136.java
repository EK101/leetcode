package LeetCode;

import java.util.Scanner;
import java.util.Map;
import java.util.HashMap;

public class LeetCode_136 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String inputStrArray = scanner.nextLine();

        int[] intArray = convertToArray(inputStrArray);
        System.out.println(singleNumber(intArray));
    }

    public static int singleNumber(int[] nums){

        Map<Integer, Integer> mapArray = countElements(nums);
        int intResult = getNumber(mapArray);
        return intResult;
    }

    public static int[] convertToArray(String stringArr) {

        String strArr = stringArr.substring(1, stringArr.length() - 1);
        String[] strValues = strArr.split(",");
        int[] intArray = new int[strValues.length];

        for (int i = 0; i < strValues.length; i++) {
            intArray[i] = Integer.parseInt(strValues[i].trim());
        }

        return intArray;
    }

    public static Map<Integer, Integer> countElements(int[] intArray) {

        Map<Integer, Integer> mapArray = new HashMap<>();

        for (int element : intArray) {
            if (mapArray.get(element) == null) {
                mapArray.put(element, 1);
            }
            else {
                mapArray.put(element, mapArray.get(element) + 1);
            }
        }
        return mapArray;
    }

    public static int getNumber(Map<Integer, Integer> mapArray){

        for (Map.Entry<Integer, Integer> entry: mapArray.entrySet()) {
            Integer key = entry.getKey();
            Integer value = entry.getValue();
            if (value == 1) {
                return key;
            }
        }
        return 0;
    }
}
