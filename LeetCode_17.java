package LeetCode;

import javax.swing.*;
import java.util.*;


public class LeetCode_17 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String stringNumber = scanner.nextLine();

        System.out.println(prepareMatrix(stringNumber));
    }

    public static List<String> prepareMatrix(String digits) {

        if (digits.isEmpty()) {
            return Collections.emptyList();
        }

        Map<Character, List<String>> refMap = new HashMap<>();

        refMap.put('2', new ArrayList<>(List.of("a", "b", "c")));
        refMap.put('3', new ArrayList<>(List.of("d", "e", "f")));
        refMap.put('4', new ArrayList<>(List.of("g", "h", "i")));
        refMap.put('5', new ArrayList<>(List.of("j", "k", "l")));
        refMap.put('6', new ArrayList<>(List.of("m", "n", "o")));
        refMap.put('7', new ArrayList<>(List.of("p", "q", "r", "s")));
        refMap.put('8', new ArrayList<>(List.of("t", "u", "v")));
        refMap.put('9', new ArrayList<>(List.of("w", "x", "y", "z")));

        List<String> result = new ArrayList<>();
        result.addAll(refMap.get(digits.charAt(0)));

        for (int i = 1; i < digits.length(); i++) {
            result.addAll(allElementsOfTwoLists(result.subList(0, result.size()), refMap.get(digits.charAt(i))));
        }

        int count = 1;

        for (int i = 0; i < digits.length(); i++) {
            count *= refMap.get(digits.charAt(i)).size();
        }

        System.out.println("Result: " + result);
        return result.subList(result.size() - count, result.size());
    }
    public static List<String> allElementsOfTwoLists(List<String> list1, List<String> list2) {

        List<String> combinedList = new ArrayList<>();

        for (int i = 0; i < list1.size(); i++) {
            for (int j = 0; j < list2.size(); j++) {
                combinedList.add(list1.get(i) + list2.get(j));
            }
        }
        return combinedList;
    }
}

