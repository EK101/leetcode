package LeetCode;

import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;

public class LeetCode_171 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String columnToTitle = scanner.nextLine();

        int result = titleToNumber(columnToTitle);

        System.out.println(result);
    }

    public static int getColumnNumber(char column) {

        Map<Character, Integer> firstLetters = new HashMap<>();

        firstLetters.put('A', 1);
        firstLetters.put('B', 2);
        firstLetters.put('C', 3);
        firstLetters.put('D', 4);
        firstLetters.put('E', 5);
        firstLetters.put('F', 6);
        firstLetters.put('G', 7);
        firstLetters.put('H', 8);
        firstLetters.put('I', 9);
        firstLetters.put('J', 10);
        firstLetters.put('K', 11);
        firstLetters.put('L', 12);
        firstLetters.put('M', 13);
        firstLetters.put('N', 14);
        firstLetters.put('O', 15);
        firstLetters.put('P', 16);
        firstLetters.put('Q', 17);
        firstLetters.put('R', 18);
        firstLetters.put('S', 19);
        firstLetters.put('T', 20);
        firstLetters.put('U', 21);
        firstLetters.put('V', 22);
        firstLetters.put('W', 23);
        firstLetters.put('X', 24);
        firstLetters.put('Y', 25);
        firstLetters.put('Z', 26);

        return firstLetters.get(column);
    }

    public static int titleToNumber(String columnTitle) {

        int lenColumnTitle = columnTitle.length();

        if (lenColumnTitle == 1) {
            return columnTitle.charAt(0) - 64;
        }

        double intResult = 0;

        for (int i = 0; i < columnTitle.length(); i++) {
            intResult += Math.pow(26, lenColumnTitle - 1) * (columnTitle.charAt(i) - 64);
            lenColumnTitle -= 1;
        }

//        intResult = (int) intResult + columnTitle.charAt(columnTitle.length() - 1) - 64;;

        return (int) intResult;
    }

}
