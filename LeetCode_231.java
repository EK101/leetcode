package LeetCode;

import java.util.Scanner;

public class LeetCode_231 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int number = scanner.nextInt();
        boolean result = isPowerOfTwo(number);

        System.out.println(result);

    }

    public static boolean isPowerOfTwo(int n) {

        if (n == 1) {
            return true;
        } else if (n <= 0) {
            return false;
        }

        double checkPower = Math.log(n) / Math.log(2);
        checkPower = Math.round(checkPower * 10000.0) / 10000.0;

        return checkPower == (int) checkPower;
    }
}