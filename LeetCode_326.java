package LeetCode;

public class LeetCode_326 {

    public static void main(String[] args) {


    }

    public static boolean isPowerOfThree(int n) {

        if (n == 1) {
            return true;
        } else if (n <= 0) {
            return false;
        }

        double checkPower = Math.log(n) / Math.log(2);
        checkPower = Math.round(checkPower * 100000000000.0) / 100000000000.0;

        return checkPower == (int) checkPower;
    }
}
