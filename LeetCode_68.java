package LeetCode;// Hard

// There is still a lot of space to optimize the solution;
// Might come back to it later.


import java.util.ArrayList;
import java.util.List;

public class LeetCode_68 {
    public static void main(String[] args) {


        String[] test1 = {"This", "is", "an", "example", "of", "text", "justification."};
        int test1Width = 16;
        String[] test2 = {"What", "must", "be", "acknowledgment", "shall", "be"};
        int test2Width = 16;
        String[] test3 = {"Science", "is", "what", "we", "understand", "well", "enough", "to", "explain", "to", "a",
                "computer.", "Art", "is", "everything", "else", "we", "do"};
        int test3Width = 20;
        String[] test4 = {"The","important","thing","is","not","to","stop","questioning.","Curiosity","has","its","own",
                "reason","for","existing."};
        int test4Width = 17;
        String[] test5 = {"ask","not","what","your","country","can","do","for","you","ask","what","you","can","do","for","your","country"};
        int test5Width = 16;
        String[] test6 = {"Give","me","my","Romeo;","and,","when","he","shall","die,","Take","him","and","cut","him","out","in","little","stars,","And","he","will","make","the","face","of","heaven","so","fine","That","all","the","world","will","be","in","love","with","night","And","pay","no","worship","to","the","garish","sun."};
        int test6Width = 25;
        String[] test7 = {"What","must","be","acknowledgment","abilities","be","abilities","be"};
        int test7Width = 16;

        List<String> result1 = constructPhrase(sortWords(test1, test1Width), test1Width);
        List<String> result2 = constructPhrase(sortWords(test2, test2Width), test2Width);
        List<String> result3 = constructPhrase(sortWords(test3, test3Width), test3Width);
        List<String> result4 = constructPhrase(sortWords(test4, test4Width), test4Width);
        List<String> result5 = constructPhrase(sortWords(test5, test5Width), test5Width);
        List<String> result6 = constructPhrase(sortWords(test6, test6Width), test6Width);
        List<String> result7 = constructPhrase(sortWords(test7, test7Width), test7Width);

        System.out.println(result1);
        System.out.println(result2);
        System.out.println(result3);
        System.out.println(result4);
        System.out.println(result5);
        System.out.println(result6);
        System.out.println(result7);
    }

    public static List<List<String>> sortWords(String[] words, int maxWidth) {

        List<List<String>> outcomeList = new ArrayList<>(maxWidth);

        int phraseIndex = 0;
        int countWords = 0;
        int phraseLen = 0;

        outcomeList.add(new ArrayList<>());

        for (int i = 0; i < words.length; i++) {

            countWords = countWords + 1;
            phraseLen = phraseLen + words[i].length();
            outcomeList.get(phraseIndex).add(words[i]);

            if (countWords == 1 && phraseLen == maxWidth) {

                outcomeList.add(new ArrayList<>());
                countWords = 0;
                phraseLen = 0;

                phraseIndex = phraseIndex + 1;

            } else if (phraseLen + countWords - 1 == maxWidth) {

                outcomeList.add(new ArrayList<>());
                countWords = 0;
                phraseLen = 0;

                phraseIndex = phraseIndex + 1;

            } else if (phraseLen + countWords - 1 > maxWidth) {

                outcomeList.get(phraseIndex).remove(outcomeList.get(phraseIndex).size() - 1);

                outcomeList.add(new ArrayList<>());

                countWords = 0;
                phraseLen = 0;
                i = i - 1;

                phraseIndex = phraseIndex + 1;
            }

        }
        return outcomeList;
    }

    public static List<String> constructPhrase(List<List<String>> words, int maxWidth) {

        List<String> result = new ArrayList<>();

        for (int p = 0; p < words.size(); p++) {

            String sentence = "";
            int phraseLen = 0;
            int countWords = words.get(p).size();

            for (String word : words.get(p)) {
                phraseLen = phraseLen + word.length();
            }

            int TotalSpaces = maxWidth - phraseLen;

            if (words.get(p).isEmpty()) {
                continue;
            }

            if (p == words.size() - 1) {
                System.out.println("Last words.get(p): " + words.get(p));
                for (int i = 0; i < countWords; i++) {
                    sentence = sentence + words.get(p).get(i) + " ";
                    TotalSpaces = TotalSpaces - 1;
                }
                sentence = sentence + " ".repeat(TotalSpaces);
                result.add(sentence);
                return result;
            }

            if (countWords == 1) {
                sentence = words.get(p).get(0) + " ".repeat(TotalSpaces);
                result.add(sentence);
            } else if (countWords == 2) {
                sentence = words.get(p).get(0) + " ".repeat(TotalSpaces) + words.get(p).get(1);
                result.add(sentence);
            } else if (TotalSpaces % (countWords - 1) == 0) {
                int interSpaces = (int) (double) TotalSpaces / (countWords - 1);
                for (int i = 0; i < countWords - 1; i++) {
                    sentence = sentence + words.get(p).get(i) + " ".repeat(interSpaces);
                }
                sentence = sentence + words.get(p).get(countWords - 1);
                result.add(sentence);
            } else {
                int interSpaces = (int) Math.ceil((double) TotalSpaces / (countWords - 1));
                int remainWords = countWords - 1;
                for (int i = 0; i < countWords; i++) {
                    sentence = sentence + words.get(p).get(i) + " ".repeat(interSpaces);
                    TotalSpaces = TotalSpaces - interSpaces;
                    remainWords = remainWords - 1;
                    if (remainWords > 0) {
                        if (TotalSpaces % remainWords == 0) {
                            interSpaces = (int) (double) TotalSpaces / remainWords;
                        }
                    }
                    else {
                        if (interSpaces > TotalSpaces) {
                            interSpaces = TotalSpaces;
                        } else if (interSpaces == TotalSpaces) {
                            interSpaces = 1;
                        } else if (TotalSpaces == (countWords - 2 - i)) {
                            interSpaces = 1;
                        }
                    }
                }
                result.add(sentence);
            }
        }
        return result;
    }
}
