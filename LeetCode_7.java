package LeetCode;

import java.util.Scanner;

public class LeetCode_7 {

    public static void main(String[] args) {

        LeetCode_7 instance = new LeetCode_7();
        Scanner scanner = new Scanner(System.in);

        int test1 = scanner.nextInt();

        System.out.println(instance.reverse(test1));
    }

    public int reverse(int x) {

        String reversedX = "";

        String intAsString = Integer.toString(x);

        for (int i = intAsString.length() - 1; i >= 0; i--) {
            reversedX = reversedX + intAsString.charAt(i);
        }

        int result = 0;
        try {
            if (x < 0) {
                result = Integer.parseInt("-" + reversedX.substring(0, reversedX.length() - 1));
            } else {
                result = Integer.parseInt(reversedX);
            }
        } catch (Exception e) {
            result = 0;
        }
        return result;
    }
}
